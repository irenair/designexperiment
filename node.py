def isFloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False

def isNone(value):
    return value == "None"

def isTrue(value):
    return value == "True"

def isOpenBracket(value):
    return value == "{"

class Node:
    def __init__(self, label = None):
        # Node Information
        self.attribute = None
        self.attribute_values = []
        self.label = label
        self.children = {}
        self.parrent_attribute = None
        self.parrent_attribute_value = None
        self.depth = 0
        self.continue_attribute = False
        self.border = None
        self.is_pruned = False
    
    def writeNode(self,fileHandler):
        fileHandler.write(str(self.attribute)+'\n')
        fileHandler.write((','.join(str(elmt) for elmt in self.attribute_values))+"\n")
        fileHandler.write(str(self.label)+'\n')
        fileHandler.write(('\n'.join([str(self.parrent_attribute),str(self.parrent_attribute_value),
                            str(self.depth),str(self.continue_attribute),str(self.border),str(self.is_pruned)]))+'\n')
        for key,value in self.children.items():
            fileHandler.write("{\n")
            fileHandler.write(str(key)+"\n")
            value.writeNode(fileHandler)
            fileHandler.write("}\n")

    def writeFile(self,filename):
        with open(filename,'w') as fileHandler:
            self.writeNode(fileHandler)

    def copy(self,node):
        self.attribute = node.attribute
        self.attribute_values = node.attribute_values
        self.label = node.label
        self.children = node.children
        self.parrent_attribute = node.parrent_attribute
        self.parrent_attribute_value = node.parrent_attribute_value
        self.depth = node.depth
        self.continue_attribute = node.continue_attribute
        self.border = node.border
        self.is_pruned = node.is_pruned

    def readFile(self,filename):
        with open(filename,'r') as fileHandler:
            data = fileHandler.readlines()
            _, node = self.readNode(data)
            self.copy(node)

    def readNode(self,data,start=0):
        node = Node()
        node.attribute = None if isNone(data[start].strip()) else data[0].strip()
        if (node.attribute != None):
            node.attribute_values = [float(elmt) if isFloat(elmt) else elmt  
            for elmt in data[start+1].strip().split(',')]
        node.label = float(data[start+2].strip()) if isFloat(data[start+2].strip()) else data[start+2].strip()
        node.parrent_attribute = None if isNone(data[start+3].strip()) else data[start+3].strip()
        if node.parrent_attribute == None:
            node.parrent_attribute_value = None
        else:
            node.parrent_attribute_value = float(data[start+4].strip()) if isFloat(data[start+4].strip()) else (data[start+4].strip())
        node.depth = float(data[start+5].strip())
        node.continue_attribute = True if isTrue(data[start+6].strip()) else False
        if isNone(data[start+7].strip()):
            node.border = None
        else:
            node.border = float(data[start+7].strip()) if isFloat(data[start+7].strip()) else (data[start+7].strip())
        node.is_pruned = True if isTrue(data[start+8].strip()) else False
        start +=9
        try:
            while (isOpenBracket(data[start].strip())):
                key = float(data[start+1].strip()) if isFloat(data[start+1].strip()) else data[start+1].strip()
                temp = Node()
                start, newNode = temp.readNode(data,start+2)
                temp.copy(newNode)
                node.children[key] = temp
        except IndexError:
            pass

        
        return start+1, node