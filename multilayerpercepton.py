import numpy as np
import pandas as pd
from io import StringIO
activation_method = ['sigmoid','softmax']

class MyMLP:
    """
        Parameters : 
            - hidden_layer_sizes: 
                tuple, each element represent the number of neuron of hidden_layer-i
            - n_iteration:
                int, represent the maximum number of iteration
            - n_batch:
                int, represent the batch size that will be used in mini-batch algorithm
            - learning_rate:
                int, represent the learning_rate that will be used in MLP
            - momentum:
                float, represent the momentum that will be used in this algoritm
            - activation:
                string, represent the moment
        Attribute : 
            - hidden_layer_sizes:
                list, each element represent the number of neuron of hidden_layer-i
                use len(hidden_layer_sizes) to get the number of layer that used in this algorithm
            - _weight : 
                list, each element (matrix) represent the weight of layer-i to layer-i+1
                each matrix element(i,j) represent the weight from neuron-i to neuron-j
            - _n_batch:
                int, represent the batch size that will be used in mini-batch algorithm
            - learning_rate:
                int, represent the learning_rate that will be used in MLP
        Method:
            - _feed_forward

            - _back_propagation

            - _update_weight
            
            - _sigmoid_function
            
    """
    def __init__(self, hidden_layer_sizes=(100,), 
                    n_iteration_=100, 
                    n_batch_=5, 
                    learning_rate=0.001,
                    activation="sigmoid",
                    threshold=0.1):
        self.hidden_layer_sizes = list(hidden_layer_sizes)
        self._weight = None
        self._d_weight = None
        self._n_batch = 5
        self._learning_rate_init = learning_rate
        self._output = []
        self._n_feature = 0
        self.activation = activation
        self._n_iteration = n_iteration_
        self._threshold = threshold

    def _get_Max_Iter(self):
        return self.n_iteration_

    def _encode_target(self,y):
        result = []
        for i in range(len(self._output)):
            if self._output[i]==y:
                result.append(1)
            else:
                result.append(0)
        return result

    def _encode_target_batch(self,y):
        encoded_y = np.array([self._encode_target(elmt) for elmt in y])
        return encoded_y

    def _validate_input(self, X, y, activation):
        if self.activation not in activation_method:
            raise ValueError('Invalid activation method')
        elif self.activation == activation_method[0]:
            self.activation_func = self._sigmoid
        else:
            self.activation_func = self._softmax
        self._n_feature = X[0].shape[0]
        self._output = np.unique(y)

    def _initialize(self,randoms=False):
        layer_sizes = [self._n_feature]+self.hidden_layer_sizes+[len(self._output)]
        np.random.seed()
        if randoms:
            weight = [np.random.rand(i+1,j) for i,j in zip(layer_sizes[:-1],layer_sizes[1:])]
        else:
            weight = [np.zeros((i+1,j)) for i,j in zip(layer_sizes[:-1],layer_sizes[1:])]
        return weight
    
    def _update_weight(self, gradients, outputs):
        d_weights = [np.append(np.dot(np.transpose(output),gradient) , np.sum(gradient,axis=0,keepdims=True),axis=0)*self._learning_rate_init for output,gradient in zip(outputs[:-1], gradients)]
        self._weight = [weight+d_weight for weight,d_weight in zip(self._weight,d_weights)]
        self._d_weight = d_weights

    def _add_bias(self,data):
        # Add bias
        row = len(data)
        col, h = 1, row
        bias = [[1 for x in range(col)] for y in range(h)] 
        data = np.append(data,bias,axis=1)
        return data
        
    def _feed_forward(self,data):
        result = []
        result.append(data)
        for weight in range(len(self._weight)):
            # Add bias to data
            data = self._add_bias(data) 

            # Count net
            temp = np.dot(data,self._weight[weight])

            # Sigmoid
            temp = np.vectorize(self.activation_func)(temp)

            result.append(temp)
            # Use result of this layer as input for the next layer
            data = temp
        return result, result[-1]
    
    def _calculate_error(self,output,target):
        d_err = 0
        target = self._encode_target_batch(target)
        for out,tar in zip(output,target):
            out = np.array(out)
            tar = np.array(tar)
            d_err += np.sum(np.power((tar-out),2))
        return d_err/2

    def _backwardpropagation(self, X,y):
        sum_err = 0
        for i in range(0,len(X),self._n_batch):
            datas = X[i:i+self._n_batch]
            targets = y[i:i+self._n_batch]
            output_list, output = self._feed_forward(datas)
            gradient_list = self._backward_phase(output_list,targets)
            self._update_weight(gradient_list,output_list)
            sum_err +=self._calculate_error(output,targets)
        return sum_err
        
    def _backward_phase(self,output,targets):
        targets = self._encode_target_batch(targets)
        gradient_list =[]
        gradient = output[-1]*(1-output[-1])*(targets-output[-1])
        gradient_list.append(gradient)
        for weight,output in zip(self._weight[1:][::-1],output[1:-1][::-1]):
            gradient = output*(1-output)*np.transpose(np.dot(weight[:-1],np.transpose(gradient_list[-1])))
            gradient_list.append(gradient)
        return gradient_list[::-1]

    def _sigmoid(self,net):
        return(1/(1+np.exp(-net)))

    def _back_propagation(self, list_of_outputs, tars):
        """
            1. receive list of outputs (matrix)
            2. for each in list of outputs
                    compute error of output
            3. for each in hidden layer
                    compute erro
                    r of output of h
            4. compute delta weight
        """
        list_of_delta = []
        delta_outs = []
        outs = list_of_outputs[len(list_of_outputs)-1]
        #compute delta for each output
        for out, tar in zip(outs,tars):
            delta_out = []
            for o, t in (zip(out,tar)):
                delta = o*(1-o)*(t-o)
                delta_out.append(delta)
            delta_outs.append(delta_out)
        list_of_delta.insert(0,delta_outs)
        
        #compute delta for hidden unit
        layer = len(list_of_outputs)-2 #karena dimulai dari nol dan list paling akhir tidak termasuk.
        while layer >=0 : 
            row = 0
            delta_outs = []
            while row <=(len(list_of_outputs[layer])-1) :
                delta_out = []
                for idxH in range(len(list_of_outputs[layer][row])):
                    w = (self._weight[layer][idxH])
                    d = (list_of_delta[0][row])
                    sum = np.dot(d,w)
                    h = list_of_outputs[layer][row][idxH]
                    delta = h*(1-h)*sum
                    delta_out.append(delta)
                delta_outs.append(delta_out)
                row += 1
            list_of_delta.insert(0,delta_outs)
            layer -= 1        
        print(list_of_delta)

    def _softmax(self,net,output):
        return np.exp(-net)/np.sum([np.exp(-elmt) for elmt in output])
        
    def fit(self,X,y):
        self._validate_input(X,y,self.activation)
        if self._weight == None:
            self._weight = self._initialize(randoms=True)
        self._d_weight = self._initialize()
        for i in range(self._n_iteration):
            err = self._backwardpropagation(X,y)
            # print(err)
            if err < self._threshold:
                break
    
    def _step_function(self, output):
        return [1 if elmt >= 0.5 else 0 for elmt in output]

    def __decode_output(self, output):
        # print(output)
        return [self._output[i] for i in range(len(self._output)) if output[i] == 1]

    def predict(self,data):
        _, output = self._feed_forward([data])
        output = self._step_function(output[0])
        out  = self.__decode_output(output)
        # print(out)
        return out
    
    def accuracy(self,datas,targets):
        count = 0
        for data, target in zip(datas,targets):
            output = self.predict(data)
            # print(target, output)
            if target in self.predict(data):
                count+=1
        return count/len(datas)
    
    def accuracy_cross_validation(self, accuracy, k):
        acv = 0
        for value in accuracy:
            acv += value
        return acv/k

    def writeFile(self, filename):
        with open(filename, 'w') as fileHandle:
            fileHandle.write(','.join(str(neuron_elmt) for neuron_elmt in self.hidden_layer_sizes))
            fileHandle.write('\n')
            fileHandle.write(str(self._n_feature)+"\n")
            fileHandle.write(','.join(str(output_elmt) for output_elmt in self._output))
            fileHandle.write('\n')
            for weight in self._weight:
                np.savetxt(fileHandle,weight,delimiter=',')
                # fileHandle.write(','.join(str(weight_elmt) for weight_elmt in weight))
            fileHandle.write(str(self._n_batch)+"\n")
            fileHandle.write(str(self._learning_rate_init)+"\n")
            fileHandle.write(self.activation+"\n")
            fileHandle.write(str(self._n_iteration)+"\n")
            fileHandle.write(str(self._threshold)+"\n")

    def readFile(self, filename):
        with open(filename, 'r') as fileHandle:
            data = fileHandle.readlines()
            self.hidden_layer_sizes = [int(elmt) for elmt in data[0].strip().split(',')]
            self._n_feature = int(data[1])
            self._output = [int(elmt) for elmt in data[2].strip().split(',')]
            layer_sizes = [self._n_feature]+self.hidden_layer_sizes+[len(self._output)]
            start = 3
            self._weight = []
            for i, j in zip(layer_sizes[:-1],layer_sizes[1:]):
                string = StringIO(u"".join(data[start:start+i+1]))
                self._weight.append(np.loadtxt(string,delimiter=","))
                start+=i+1
            self._n_batch = int(data[-5].strip())
            self._learning_rate_init = float(data[-4].strip())
            self.activation = data[-3].strip()
            self._n_iteration = int(data[-2].strip())
            self._threshold = float(data[-1].strip())

    def export_text(self):
        counter = 0
        string = ""
        for weight in self._weight:
            if counter == 0:
                string += "Input Layer --> Hidden Layer 1\n"
            elif counter == len(self._weight)-1:
                string += "Hidden Layer "+str(counter)+" --> Output Layer"+"\n"
            else:
                string+="Hidden Layer "+ str(counter)+" --> Hidden Layer "+ str(counter+1)+"\n"
            for i in range(len(weight)):
                string+="|--- "
                if i == len(weight)-1:
                    string+="Bias\n"
                else:
                    string+="Neuron - "+str(i+1)+"\n"
                for j in range(len(weight[i])):
                    string+="|------ "+"Neuron "+str(j)+" : "+str(weight[i][j])+"\n"
            counter+=1
        return string