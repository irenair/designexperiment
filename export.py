from node import Node

def export_text(dec_tree, depth=0):
    text = ""
    # print(dec_tree.attribute)
    if len(dec_tree.children)==0 or dec_tree.is_pruned:
        for i in range(depth):
            text+="|   "
        text += "|--- "
        text+="Class : " + str(dec_tree.label) +"\n"
    else:
        if dec_tree.is_pruned :
            return ""
        else:
            # print(depth,dec_tree.attribute_values)
            for attribute_value in dec_tree.attribute_values:
                for i in range(depth):
                    text+="|   "
                text += "|--- "
                if dec_tree.continue_attribute:
                    if attribute_value == 'more':
                        # print("more")
                        text +=str(dec_tree.attribute) + " > " + str(dec_tree.border) +"\n"
                    else:
                        # print("less")
                        text +=str(dec_tree.attribute) + " <= " + str(dec_tree.border)+"\n"
                else:
                    text+=str(dec_tree.attribute) + " : " + str(attribute_value) + "\n"
                text+=export_text(dec_tree.children[attribute_value], depth+1)
    return text